from azure.cosmos.exceptions import CosmosResourceNotFoundError
from flask import current_app

class Item:
    container_name = 'items'
    
    def __init__(self, name, shop, cost):
        self.id = None
        self.name = name
        self.shop = shop
        self.cost = cost;

    def save(self):
        client = current_app.config['COSMOS_CLIENT']
        database_name = current_app.config['COSMOS_DATABASE_NAME']
        database = client.get_database_client(database_name)
        container = database.get_container_client(self.container_name)
        
        item = {
            'name': self.name,
            'shop': self.shop,
            'cost': self.cost
        }

        if self.id:
            try:
                existing_item = container.read_item(item_id=self.id, partition_key=item['name'])
            except CosmosResourceNotFoundError:
                pass
            else:
                item['id'] = self.id
                item['_etag'] = existing_item['_etag']
                container.replace_item(item=item, item_id=self.id, partition_key=item['name'])
                return

        response = container.create_item(item=item)
        self.id = response['id']

    @classmethod
    def get_all(cls):
        client = current_app.config['COSMOS_CLIENT']
        database_name = current_app.config['COSMOS_DATABASE_NAME']
        database = client.get_database_client(database_name)
        container = database.get_container_client(cls.container_name)
        
        items = container.query_items(
            query="SELECT * FROM items",
            enable_cross_partition_query=True
        )
        
        return [Item(**item) for item in items]

    @classmethod
    def delete_all(cls):
        client = current_app.config['COSMOS_CLIENT']
        database_name = current_app.config['COSMOS_DATABASE_NAME']
        database = client.get_database_client(database_name)
        container = database.get_container_client(cls.container_name)
        
        items = cls.get_all()
        
        for item in items:
            container.delete_item(item=item.id, partition_key=item.name)
